// This file is part of the Vocnotent program.
// Copyright (C) 2022 Tobias Kannenberg
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use psimple::Simple;
use pulse::def::BufferAttr;
use pulse::sample::{Format, Spec};
use pulse::stream::Direction;

use std::error::Error;

pub struct AudioStream {
    /// frames per second
    pub frame_rate: u32,
    /// samples per buffer
    pub buffer_size: u32,
    stream: Option<Simple>,
}

impl AudioStream {
    pub fn new(frame_rate: u32, buffer_size: u32) -> Self {
        AudioStream {
            frame_rate,
            buffer_size,
            stream: None,
        }
    }

    pub fn start(&mut self) -> Result<(), Box<dyn Error>> {
        let spec = Spec {
            format: Format::F32le,
            channels: 1,
            rate: self.frame_rate,
        };
        assert!(spec.is_valid());

        let attr = BufferAttr {
            maxlength: self.buffer_size * spec.sample_size() as u32,
            ..BufferAttr::default()
        };

        self.stream = Some(Simple::new(
            // server
            None,
            // name
            "Vocnotent",
            // direction
            Direction::Record,
            // device (None -> default)
            None,
            // stream name
            "input",
            // sample specification
            &spec,
            // channel map (None -> default)
            None,
            // buffering attributes (None -> default)
            Some(&attr),
        )?);

        Ok(())
    }

    pub fn stop(&mut self) {
        self.stream = None;
    }

    pub fn read(&mut self, buffer: &mut [u8]) -> Result<(), Box<dyn Error>> {
        self.stream
            .as_ref()
            .ok_or("Can't read from stream. Please call the start() method first.")?
            .read(buffer)?;
        Ok(())
    }
}
