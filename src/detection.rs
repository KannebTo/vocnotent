// This file is part of the Vocnotent program.
// Copyright (C) 2022 Tobias Kannenberg
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use pitch_detection::detector::PitchDetector;
use pitch_detection::detector::mcleod::McLeodDetector;
use pitch_detection::detector::internals::Pitch;

use crate::Config;

#[derive(Clone, PartialEq, Debug)]
pub enum NoteStatus {
    StartNote(u8),
    HoldNote(u8),
    StopNote(u8),
    NoNote,
}

pub struct NoteDetector {
    pitch_detector: McLeodDetector<f32>,
    note: u8,
    note_status: NoteStatus,
    note_duration: f32,
    gap_duration: f32,
    config: Config,
}

fn frequency_to_note(f: f32, concert_pitch: f32) -> u8 {
    assert!(f.is_sign_positive());
    assert!(concert_pitch.is_sign_positive());
    assert!(concert_pitch > 0.0);

    // see https://en.wikipedia.org/wiki/MIDI_tuning_standard
    (69.0 + (12.0 * (f / concert_pitch).log2()).round()) as u8
}

impl NoteDetector {

    pub fn new(config: Config) -> Self {
        NoteDetector {
            pitch_detector: McLeodDetector::<f32>::new(config.buffer_size as usize, 0),
            note: 255,
            note_status: NoteStatus::NoNote,
            note_duration: 0.0,
            gap_duration: config.min_gap,
            config,
        }
    }

    fn detect_pitch(&mut self, data: &[f32]) -> Option<Pitch<f32>> {
        self.pitch_detector.get_pitch(
            data, self.config.sample_rate as usize,
            self.config.power_threshold, self.config.clarity_threshold)
    }

    fn process_pitch(&mut self, pitch: Option<Pitch<f32>>) {
        let buffer_duration = self.config.buffer_size as f32 / self.config.sample_rate as f32;

        match pitch {
            Some(pitch) => {
                let note = (
                    frequency_to_note(pitch.frequency, self.config.concert_pitch) as i16 +
                    self.config.offset as i16
                ).min(127).max(0) as u8;

                match self.note_status {
                    NoteStatus::NoNote | NoteStatus::StopNote(_) => {
                        if note == self.note {
                            self.note_duration += buffer_duration;
                            self.gap_duration += buffer_duration;
                            if self.note_duration >= self.config.min_duration &&
                                self.gap_duration >= self.config.min_gap {

                                self.note_status = NoteStatus::StartNote(note);
                            } else {
                                self.note_status = NoteStatus::NoNote;
                            }
                        } else {
                            self.note_status = NoteStatus::NoNote;
                            self.note_duration = buffer_duration;
                        }
                    },
                    NoteStatus::StartNote(n) | NoteStatus::HoldNote(n) => {
                        if n == note {
                            self.note_status = NoteStatus::HoldNote(n);
                        } else {
                            self.note_status = NoteStatus::StopNote(n);
                            self.note_duration = buffer_duration;
                            self.gap_duration = 0.0;
                            self.note = 255;
                        }
                    }
                }
                self.note = note;
            }
            None => {
                match self.note_status {
                    NoteStatus::StartNote(n) | NoteStatus::HoldNote(n) => {
                        self.note_status = NoteStatus::StopNote(n);
                        self.note_duration = 0.0;
                        self.gap_duration = buffer_duration;
                        self.note = 255;
                    },
                    NoteStatus::StopNote(_) => {
                        self.note_status = NoteStatus::NoNote;
                    },
                    NoteStatus::NoNote => {
                        if self.gap_duration < self.config.min_gap {
                            self.gap_duration += buffer_duration;
                        }
                    }
                }
            }
        }
    }

    pub fn detect_note(&mut self, data: &[f32]) -> NoteStatus {
        let pitch = self.detect_pitch(data);

        self.process_pitch(pitch);

        self.note_status.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pitch_conversion() {
        assert_eq!(frequency_to_note(440.0, 440.0), 69);
        assert_eq!(frequency_to_note(440.5, 440.0), 69);
        assert_eq!(frequency_to_note(220.0, 440.0), 69 - 12);
        assert_eq!(frequency_to_note(  0.0, 440.0), 0);
    }

    #[test]
    #[should_panic]
    fn test_pitch_conversion_zero_cp() {
        frequency_to_note(440.0, 0.0);
    }

    #[test]
    #[should_panic]
    fn test_pitch_conversion_neg_f() {
        frequency_to_note(-440.0, 440.0);
    }

    #[test]
    #[should_panic]
    fn test_pitch_conversion_neg_cp() {
        frequency_to_note(440.0, -440.0);
    }

    fn new_detector(offset: i8, min_gap: f32) -> NoteDetector {
        NoteDetector::new(Config {
            sample_rate: 10,
            buffer_size: 10,
            min_duration: 1.1,
            min_gap,
            offset,
            ..Config::default()
        })
    }

    #[test]
    fn test_state_logic_start_stop() {
        let mut d = new_detector(0, 0.0);

        // start with NoNote state
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(None);
        // rest on NoNote
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        // note not long enougth (1.0 s)
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        // note is long enougth (2.0 s)
        assert_eq!(d.note_status, NoteStatus::StartNote(69));
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::HoldNote(69));
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::HoldNote(69));
        d.process_pitch(None);
        assert_eq!(d.note_status, NoteStatus::StopNote(69));
        d.process_pitch(None);
        assert_eq!(d.note_status, NoteStatus::NoNote);
    }

    #[test]
    fn test_state_logic_start_unstable() {
        let mut d = new_detector(0, 0.0);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        // note not long enougth (1.0 s)
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(Some(Pitch{frequency: 220.0, clarity: 1.0}));
        // new pitch -> reset to 1.0 s
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        // new pitch -> reset to 1.0 s
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69));
    }

    #[test]
    fn test_offset_positive() {
        let mut d = new_detector(52, 0.0);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69 + 52));
    }

    #[test]
    fn test_offset_positive2() {
        let mut d = new_detector(100, 0.0);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        // clip to highest posible MIDI note
        assert_eq!(d.note_status, NoteStatus::StartNote(127));
    }

    #[test]
    fn test_offset_negative() {
        let mut d = new_detector(-34, 0.0);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69 - 34));
    }

    #[test]
    fn test_offset_negative2() {
        let mut d = new_detector(-100, 0.0);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        // clip to lowest posible MIDI note
        assert_eq!(d.note_status, NoteStatus::StartNote(0));
    }

    #[test]
    fn test_state_logic_change_pitch() {
        let mut d = new_detector(0, 0.0);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69));
        // change pitch
        d.process_pitch(Some(Pitch{frequency: 220.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StopNote(69));
        d.process_pitch(Some(Pitch{frequency: 220.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69 - 12));
    }

    #[test]
    fn test_state_logic_gap() {
        let mut d = new_detector(0, 2.1);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69));
        d.process_pitch(None);
        assert_eq!(d.note_status, NoteStatus::StopNote(69));
        // gap: 1.0 s
        d.process_pitch(None);
        assert_eq!(d.note_status, NoteStatus::NoNote);
        // gap: 2.0 s; duration: 1.0 s
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        // gap: 3.0 s -> OK; duration: 2.0 s
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        // gap: 4.0 s -> OK; duration: 3.0 s -> OK
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69));
    }

    #[test]
    fn test_state_logic_gap2() {
        let mut d = new_detector(0, 2.1);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69));
        d.process_pitch(None);
        assert_eq!(d.note_status, NoteStatus::StopNote(69));
        // gap: 1.0 s; duration: 1.0 s
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        // gap: 2.0 s; duration: 2.0 s
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        // gap: 3.0 s -> OK; duration: 3.0 s -> OK
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69));
    }

    #[test]
    fn test_state_logic_gap3() {
        let mut d = new_detector(0, 2.1);

        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        d.process_pitch(Some(Pitch{frequency: 440.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69));
        // change pitch
        d.process_pitch(Some(Pitch{frequency: 220.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StopNote(69));
        // gap: 1.0 s; duration: 2.0 s
        d.process_pitch(Some(Pitch{frequency: 220.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        // gap: 2.0 s; duration: 3.0 s -> OK
        d.process_pitch(Some(Pitch{frequency: 220.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::NoNote);
        // gap: 3.0 s -> OK; duration: 3.0 s -> OK
        d.process_pitch(Some(Pitch{frequency: 220.0, clarity: 1.0}));
        assert_eq!(d.note_status, NoteStatus::StartNote(69 - 12));
    }
}
