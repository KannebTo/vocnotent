// This file is part of the Vocnotent program.
// Copyright (C) 2022 Tobias Kannenberg
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use argopt::cmd;

use vocnotent::audio::AudioStream;
use vocnotent::detection::{NoteDetector, NoteStatus};
use vocnotent::output::NoteOutput;
use vocnotent::Config;

fn setup_ctrlc_handler() -> Arc<AtomicBool> {
    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();

    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");

    running
}

// TODO: find a way to set the default values based on Config::default()
#[cmd]
fn main(
    /// verbose output
    #[opt(short, long)]
    verbose: bool,
    /// transpose note <offset> semitones
    #[opt(short, long, default_value = "0")]
    offset: i8,
    /// sample rate of audio stream
    #[opt(long, default_value = "48000")]
    sample_rate: u32,
    #[opt(long, default_value = "4096")]
    /// buffer size (samples per buffer)
    buffer_size: u32,
    #[opt(long, default_value = "0.2")]
    /// power threahold for pitch detection [0.0, 1.0]
    power_threshold: f32,
    /// clarity threshold for pitch detection [0.0, 1.0]
    #[opt(long, default_value = "0.7")]
    clarity_threshold: f32,
    /// minimal duration (in seconds) on constant note to start the note
    #[opt(long, default_value = "0.2")]
    min_duration: f32,
    /// minimal gap (in seconds) between notes
    #[opt(long, default_value = "0.5")]
    min_gap: f32,
    /// concert pitch in Hz
    #[opt(long, default_value = "440.0")]
    concert_pitch: f32,
) {
    let config = Config {
        verbose,
        offset,
        sample_rate,
        buffer_size,
        power_threshold,
        clarity_threshold,
        min_duration,
        min_gap,
        concert_pitch,
    };

    if config.verbose {
        println!("config: {:#?}", config);
    }

    let mut output = NoteOutput::new().expect("midi init error");
    let mut detector = NoteDetector::new(config.clone());

    let mut stream = AudioStream::new(config.sample_rate, config.buffer_size);
    stream.start().expect("audio stream initialization failed!");
    const BIT_DEPTH: usize = 4;
    let mut buffer = vec![0u8; BIT_DEPTH * config.buffer_size as usize];

    let mut note_status = NoteStatus::NoNote;

    let running = setup_ctrlc_handler();

    println!("Press Ctrl-C to quit");
    while running.load(Ordering::SeqCst) {
        match stream.read(&mut buffer) {
            Ok(_) => {
                let data: Vec<f32> = buffer
                    .chunks(BIT_DEPTH as usize)
                    .map(|e| f32::from_le_bytes(e.try_into().expect("invalid byte chunk size")))
                    .collect();

                note_status = detector.detect_note(&data);

                match note_status {
                    NoteStatus::StartNote(n) => {
                        if config.verbose {
                            println!("play note: {}", n)
                        };
                        output.play_note(n);
                    }
                    NoteStatus::StopNote(n) => {
                        if config.verbose {
                            println!("release note: {}", n)
                        };
                        output.stop_note(n);
                    }
                    _ => {}
                }
            }
            Err(e) => {
                println!("{}", e)
            }
        }
    }

    match note_status {
        NoteStatus::StartNote(n) | NoteStatus::HoldNote(n) => {
            if config.verbose {
                println!("release note: {}", n)
            };
            output.stop_note(n);
        }
        _ => {}
    }
}
