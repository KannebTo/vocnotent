// This file is part of the Vocnotent program.
// Copyright (C) 2022 Tobias Kannenberg
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use midir::{MidiOutput, MidiOutputConnection};
use std::error::Error;

const NOTE_ON_MSG: u8 = 0x90;
const NOTE_OFF_MSG: u8 = 0x80;
const VELOCITY: u8 = 0x64;

pub struct NoteOutput {
    connection: MidiOutputConnection,
}

impl NoteOutput {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        let output = MidiOutput::new("vocnotent")?;
        let ports = output.ports();
        let connection = output.connect(ports.get(0).unwrap(), "vocnotent")?;

        Ok(NoteOutput {connection})
    }

    pub fn play_note(&mut self, note: u8) {
        let _ = self.connection.send(&[NOTE_ON_MSG, note, VELOCITY]);
    }

    pub fn stop_note(&mut self, note: u8) {
        let _ = self.connection.send(&[NOTE_OFF_MSG, note, 0x00]);
    }
}
