// This file is part of the Vocnotent program.
// Copyright (C) 2022 Tobias Kannenberg
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

pub mod audio;
pub mod detection;
pub mod output;

#[derive(Clone, Debug)]
pub struct Config {
    /// transpose note (+- semitones)
    pub offset: i8,
    /// sample rate of audio stream
    pub sample_rate: u32,
    /// buffer size (samples per buffer)
    pub buffer_size: u32,
    /// power threahold for pitch detection [0.0, 1.0]
    pub power_threshold: f32,
    /// clarity threshold for pitch detection [0.0, 1.0]
    pub clarity_threshold: f32,
    /// minimal duration (in seconds) on constant note to start the note
    pub min_duration: f32,
    /// minimal gap (in seconds) between notes
    pub min_gap: f32,
    /// verbose output
    pub verbose: bool,
    /// concert pitch in Hz
    pub concert_pitch: f32,
}

impl Config {
    pub fn default() -> Self {
        Config {
            verbose: false,
            offset: 0,
            sample_rate: 48_000,
            buffer_size: (2u32).pow(12),
            power_threshold: 0.2,
            clarity_threshold: 0.7,
            min_duration: 0.2,
            min_gap: 0.5,
            concert_pitch: 440.0,
        }
    }
}
