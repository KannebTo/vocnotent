# Vocnotent
**Voc**al **Not**e **Ent**ering

This program detects the pitch of the microphone input and sends the corresponding notes as MIDI messages.
It is intended as an alternative input method for music notation software by singing the pitch rather than clicking or typing.

At the moment, this is tested on GNU/Linux with [MuseScore](https://musescore.org/) only.

![diagram](diagram.svg)

This is my first project using the [Rust](https://www.rust-lang.org/) programming language.

## Installation
- install [Rust](https://www.rust-lang.org/) (and cargo)
- clone this repository
- running from cloned directory:
  - `cargo run` builds and executes this program
- or install it for the current user:
  - `cargo install --path .`
  - make sure `$HOME/.cargo/bin/` is in your `$PATH`
  - now it should be available as command `vocnotent`

## Usage
1. start this program (`vocnotent` or `cargo run`)
2. enter note insertion mode in your music notation software with enabled MIDI input
3. select the right note length
4. insert a note by singing/humming/whistling the desired pitch
5. repeat from 3.

For additional information about flags and options, see `vocnotent --help` (or `cargo run -- --help`).

## Dependencies
- [libpulse-simple-binding](https://crates.io/crates/libpulse-simple-binding) for reading audio signal from PulseAudio
- [pitch-detection](https://crates.io/crates/pitch-detection) for pitch detection
- [midir](https://crates.io/crates/midir) for sending MIDI messages
- [argopt](https://crates.io/crates/argopt) for command line argument parsing
- [ctrlc](https://crates.io/crates/ctrlc) for a clean shutdown of the program

## Roadmap
- [ ] cross platform compatibility
  - [ ] switch to PortAudio instead of PulseAudio for audio access?
- [ ] publish a release
  - [ ] crates.io
  - [ ] AUR
- [ ] implement interactive GUI

## License
```text
Copyright (C) 2022 Tobias Kannenberg

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
```
